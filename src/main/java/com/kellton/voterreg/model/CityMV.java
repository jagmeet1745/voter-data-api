/**
 * 
 */
package com.kellton.voterreg.model;

/**
 * @author jagmeet
 *
 */
public class CityMV {
	private Long cityId;
	private String cityName;
	
	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

}
