/**
 * 
 */
package com.kellton.voterreg.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import com.kellton.voterreg.entity.City;
import com.kellton.voterreg.entity.Constituency;
import com.kellton.voterreg.entity.State;
import com.kellton.voterreg.entity.Ward;

/**
 * @author jagmeet
 *
 */
public class VoterVM {
	@NotNull
	private String voterName;
	@NotNull
	private String fatherName;
	@Email
	@NotNull
	private String email;
	@NotNull
	private String phoneNo;
	@NotNull
	private java.sql.Date dob;
	@NotNull
	private State state;
	@NotNull
	private City city;
	@NotNull
	private Constituency constituency;
	@NotNull
	private Ward ward;

	public String getVoterName() {
		return voterName;
	}

	public void setVoterName(String voterName) {
		this.voterName = voterName;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public java.sql.Date getDob() {
		return dob;
	}

	public void setDob(java.sql.Date dob) {
		this.dob = dob;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Constituency getConstituency() {
		return constituency;
	}

	public void setConstituency(Constituency constituency) {
		this.constituency = constituency;
	}

	public Ward getWard() {
		return ward;
	}

	public void setWard(Ward ward) {
		this.ward = ward;
	}

}
