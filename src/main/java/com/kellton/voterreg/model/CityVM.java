/**
 * 
 */
package com.kellton.voterreg.model;

/**
 * @author jagmeet
 *
 */
public class CityVM {
	
	private Long cityId;

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

}
