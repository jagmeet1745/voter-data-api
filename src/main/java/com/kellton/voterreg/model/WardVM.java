/**
 * 
 */
package com.kellton.voterreg.model;

/**
 * @author jagmeet
 *
 */
public class WardVM {
	private Long wardId;

	public Long getWardId() {
		return wardId;
	}

	public void setWardId(Long wardId) {
		this.wardId = wardId;
	}
}
