/**
 * 
 */
package com.kellton.voterreg.model;

/**
 * @author jagmeet
 *
 */
public class ConstituencyVM {
	private Long constituencyId;

	public Long getConstituencyId() {
		return constituencyId;
	}

	public void setConstituencyId(Long constituencyId) {
		this.constituencyId = constituencyId;
	}

}
