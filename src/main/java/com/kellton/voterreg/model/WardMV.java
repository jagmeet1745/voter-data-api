/**
 * 
 */
package com.kellton.voterreg.model;

/**
 * @author jagmeet
 *
 */
public class WardMV {
	private Long wardId;
	private String wardName;

	public Long getWardId() {
		return wardId;
	}

	public void setWardId(Long wardId) {
		this.wardId = wardId;
	}

	public String getWardName() {
		return wardName;
	}

	public void setWardName(String wardName) {
		this.wardName = wardName;
	}

}
