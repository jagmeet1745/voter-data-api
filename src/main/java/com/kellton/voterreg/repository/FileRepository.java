/**
 * 
 */
package com.kellton.voterreg.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kellton.voterreg.entity.File;

/**
 * @author jagmeet
 *
 */
public interface FileRepository extends JpaRepository<File, String> {

}
