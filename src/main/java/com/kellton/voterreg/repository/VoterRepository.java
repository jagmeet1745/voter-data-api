/**
 * 
 */
package com.kellton.voterreg.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kellton.voterreg.entity.Voter;

/**
 * @author jagmeet
 *
 */
public interface VoterRepository extends JpaRepository<Voter,Long>{

}
