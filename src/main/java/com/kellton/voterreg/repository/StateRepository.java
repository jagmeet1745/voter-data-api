/**
 * 
 */
package com.kellton.voterreg.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.kellton.voterreg.entity.State;

/**
 * @author jagmeet
 *
 */
public interface StateRepository extends JpaRepository<State, Long> {
	
	public State findByStateId(Long stateId);
}
