/**
 * 
 */
package com.kellton.voterreg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.kellton.voterreg.entity.Constituency;
import com.kellton.voterreg.entity.Ward;

/**
 * @author jagmeet
 *
 */
public interface WardRepository extends JpaRepository<Ward, Long> {
	
	public List<Ward> findByConstituency(Constituency constituency);
}
