/**
 * 
 */
package com.kellton.voterreg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.kellton.voterreg.entity.City;
import com.kellton.voterreg.entity.Constituency;

/**
 * @author jagmeet
 *
 */
public interface ConstituencyRepository extends JpaRepository<Constituency, Long>{
	
	public Constituency findByconstituencyId(Long constituencyId);
	
	public List<Constituency> findByCity(City city);

}
