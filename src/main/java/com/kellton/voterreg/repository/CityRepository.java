/**
 * 
 */
package com.kellton.voterreg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.kellton.voterreg.entity.City;
import com.kellton.voterreg.entity.State;

/**
 * @author jagmeet
 *
 */
public interface CityRepository extends JpaRepository<City, Long> {
	
	public City findByCityId(Long cityId);

	public List<City> findByState(State state);
}
