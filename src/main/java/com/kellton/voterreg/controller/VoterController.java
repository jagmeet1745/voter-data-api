/**
 * 
 */
package com.kellton.voterreg.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.kellton.voterreg.model.VoterMV;
import com.kellton.voterreg.model.VoterVM;
import com.kellton.voterreg.service.VoterService;

/**
 * @author jagmeet
 *
 */
@RestController
@RequestMapping("/voter-registration")
public class VoterController {
	@Autowired
	private VoterService voterService;

	/***
	 * API to retrieve all voters
	 */
	@GetMapping("/voters")
	public List<VoterMV> getUsers() {
		return voterService.getAllUsers();

	}

	/***
	 * API to save voter
	 */
	@PostMapping("/save")
	public void saveVoter(@Valid @RequestBody VoterVM voterVM) {
		voterService.saveVoter(voterVM);
	}

	/***
	 * API to edit Voter
	 */
	@PutMapping("/voter/{id}")
	public ResponseEntity<Object> editVoter(@Valid @RequestBody VoterVM voterVM, @PathVariable Long id)
			throws NumberFormatException {
		return voterService.editVoter(voterVM, id);
	}

}
