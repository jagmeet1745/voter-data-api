/**
 * 
 */
package com.kellton.voterreg.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.kellton.voterreg.entity.File;
import com.kellton.voterreg.service.FileStorageService;

/**
 * @author jagmeet
 *
 */
@RestController
public class FileController {

	private static final Logger log = LoggerFactory.getLogger(FileController.class);

	@Autowired
	FileStorageService fileStorageService;

	@PostMapping("/uploadFile")
	public File uploadFile(@RequestParam("File") MultipartFile file) {
		log.info("entered file service");
		return fileStorageService.storeFile(file);

	}
}
