/**
 * 
 */
package com.kellton.voterreg.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kellton.voterreg.model.CityMV;
import com.kellton.voterreg.model.ConstituencyMV;
import com.kellton.voterreg.model.StateMV;
import com.kellton.voterreg.model.WardMV;
import com.kellton.voterreg.service.MasterService;

/**
 * @author jagmeet
 *
 */
@RestController
@RequestMapping("/master")
public class MasterController {
	private static final Logger log = LoggerFactory.getLogger(MasterController.class);

	@Autowired
	private MasterService masterService;

	/***
	 * API to retrieve all states
	 */
	@GetMapping("/states")
	public List<StateMV> getAllStates() {
		log.info("entered master controller");
		return masterService.getAllStates();
	}

	/***
	 * API to retrieve all cities for a particular state
	 */
	@GetMapping("/states/{stateId}/cities")
	public List<CityMV> getAllCities(@PathVariable Long stateId) {
		log.info("entered master controller");
		return masterService.getAllCities(stateId);
	}

	/***
	 * API to retrieve all constituencies for a particular city
	 */
	@GetMapping("/cities/{cityId}/constituencies")
	public List<ConstituencyMV> getAllConstituencies(@PathVariable Long cityId) {
		log.info("entered master controller");
		return masterService.getAllConstituencies(cityId);
	}

	/***
	 * API to retrieve all wards for a particular constituency
	 */
	@GetMapping("/constituencies/{constituencyId}/wards")
	public List<WardMV> getAllWards(@PathVariable Long constituencyId) {
		log.info("entered master controller");
		return masterService.getAllWards(constituencyId);
	}

}
