/**
 * 
 */
package com.kellton.voterreg.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import com.kellton.voterreg.model.VoterMV;
import com.kellton.voterreg.model.VoterVM;

/**
 * @author jagmeet
 *
 */
public interface VoterService {

	/**
	 * @return
	 */
	public List<VoterMV> getAllUsers();

	public void saveVoter(VoterVM voterVM);

	public ResponseEntity<Object> editVoter(VoterVM voterVM, Long id);

}
