/**
 * 
 */
package com.kellton.voterreg.service;

import java.util.List;

import com.kellton.voterreg.model.CityMV;
import com.kellton.voterreg.model.ConstituencyMV;
import com.kellton.voterreg.model.StateMV;
import com.kellton.voterreg.model.WardMV;

/**
 * @author jagmeet
 *
 */
public interface MasterService {

	public List<StateMV> getAllStates();

	public List<CityMV> getAllCities(Long stateId);

	public List<ConstituencyMV> getAllConstituencies(Long cityId);

	public List<WardMV> getAllWards(Long constituencyId);

}
