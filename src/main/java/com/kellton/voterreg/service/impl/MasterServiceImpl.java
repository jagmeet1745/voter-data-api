/**
 * 
 */
package com.kellton.voterreg.service.impl;

import java.util.Arrays;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kellton.voterreg.entity.City;
import com.kellton.voterreg.entity.Constituency;
import com.kellton.voterreg.entity.State;
import com.kellton.voterreg.entity.Ward;
import com.kellton.voterreg.model.CityMV;
import com.kellton.voterreg.model.ConstituencyMV;
import com.kellton.voterreg.model.StateMV;
import com.kellton.voterreg.model.WardMV;
import com.kellton.voterreg.repository.CityRepository;
import com.kellton.voterreg.repository.ConstituencyRepository;
import com.kellton.voterreg.repository.StateRepository;
import com.kellton.voterreg.repository.WardRepository;
import com.kellton.voterreg.service.MasterService;

/**
 * @author jagmeet
 *
 */
@Service
public class MasterServiceImpl implements MasterService {

	private static final Logger log = LoggerFactory.getLogger(MasterServiceImpl.class);

	@Autowired
	private StateRepository stateRepository;
	@Autowired
	private CityRepository cityRepository;
	@Autowired
	private ConstituencyRepository constituencyRepository;
	@Autowired
	private WardRepository wardRepository;
	@Autowired
	private ModelMapper modelMapper;

	public List<StateMV> getAllStates() {
		log.info("entered master service");
		List<State> states = stateRepository.findAll();
		StateMV[] stateMVs = modelMapper.map(states, StateMV[].class);
		return Arrays.asList(stateMVs);

	}

	public List<CityMV> getAllCities(Long stateId) {
		log.info("entered master service");
		State state = stateRepository.findByStateId(stateId);
		List<City> cities = cityRepository.findByState(state);
		CityMV[] cityMVs = modelMapper.map(cities, CityMV[].class);
		return Arrays.asList(cityMVs);
	}

	@Override
	public List<ConstituencyMV> getAllConstituencies(Long cityId) {
		log.info("entered master service");
		City city = cityRepository.findByCityId(cityId);
		List<Constituency> constituencies = constituencyRepository.findByCity(city);
		ConstituencyMV[] constituencyMVs = modelMapper.map(constituencies, ConstituencyMV[].class);
		return Arrays.asList(constituencyMVs);
	}

	@Override
	public List<WardMV> getAllWards(Long constituencyId) {
		log.info("entered master service");
		Constituency constituency = constituencyRepository.findByconstituencyId(constituencyId);
		List<Ward> wards = wardRepository.findByConstituency(constituency);
		WardMV[] wardMVs = modelMapper.map(wards, WardMV[].class);
		return Arrays.asList(wardMVs);
	}

}
