/**
 * 
 */
package com.kellton.voterreg.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import it.ozimov.springboot.mail.model.Email;
import it.ozimov.springboot.mail.model.defaultimpl.DefaultEmail;
import javax.mail.internet.InternetAddress;
import static com.google.common.collect.Lists.newArrayList;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.kellton.voterreg.entity.Voter;
import com.kellton.voterreg.model.VoterMV;
import com.kellton.voterreg.model.VoterVM;
import com.kellton.voterreg.repository.VoterRepository;
import com.kellton.voterreg.service.VoterService;

import it.ozimov.springboot.mail.service.EmailService;

/**
 * @author jagmeet
 *
 */
@Service
public class VoterServiceImpl implements VoterService {
	@Autowired
	private VoterRepository voterRepository;
	// To map model voter entity with model view
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	public EmailService emailService;
	private static final Logger log = LoggerFactory.getLogger(VoterServiceImpl.class);

	/***
	 * Function to retreive voters
	 */
	@Override
	public List<VoterMV> getAllUsers() {
		log.info("Entered voter service");
		List<Voter> voters = voterRepository.findAll();
		VoterMV[] voterMVs = modelMapper.map(voters, VoterMV[].class);
		return Arrays.asList(voterMVs);
	}

	/***
	 * Function to Save Voter
	 */
	public void saveVoter(VoterVM voterVM) {
		log.info("Entered voter service");
		Voter voter = modelMapper.map(voterVM, Voter.class);
		if (voterRepository.save(voter) != null) {
			try {
				sendEmail(voter);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
	}

	/***
	 * Function to Edit Voter
	 */
	@Override
	public ResponseEntity<Object> editVoter(VoterVM voterVM, Long id) {
		log.info("Entered Voter Service");
		Optional<Voter> voterOptional = voterRepository.findById(id);
		if (!voterOptional.isPresent())
			return ResponseEntity.notFound().build();
		Voter voter = modelMapper.map(voterVM, Voter.class);
		voter.setVoterId(id);
		voterRepository.save(voter);
		return ResponseEntity.ok(voter);
	}

	/***
	 * Fucntion to send email
	 */
	public void sendEmail(Voter voter) throws UnsupportedEncodingException {
		// Generating unique id using UUID
		String uniqueID = UUID.randomUUID().toString();
		log.info("Sending email");
		final Email email = DefaultEmail.builder().from(new InternetAddress("jagmeet.js@gmail.com", "JAGMEET SINGH"))
				.to(newArrayList(new InternetAddress(voter.getEmail(), voter.getVoterName())))
				.subject("Enrollment Successfull")
				.body("Hello " + voter.getVoterName() + " your unique id is " + uniqueID).encoding("UTF-8").build();
		emailService.send(email);
		log.info("email sent");
	}

}
