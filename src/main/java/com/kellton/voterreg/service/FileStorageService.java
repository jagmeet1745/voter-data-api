/**
 * 
 */
package com.kellton.voterreg.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.kellton.voterreg.entity.File;
import com.kellton.voterreg.exception.FileStorageException;
import com.kellton.voterreg.property.FileStorageProperties;
import com.kellton.voterreg.repository.FileRepository;

/**
 * @author jagmeet
 *
 */
@Service
public class FileStorageService {
	@Autowired
	private FileRepository fileRepository;
	private final Path fileStorageLocation;

	public FileStorageService(FileStorageProperties fileStorageProperties) {
		this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();
		System.out.println(this.fileStorageLocation);
		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception ex) {
			throw new FileStorageException("Could not create the directory where the uploaded files will be stored.",
					ex);
		}
	}

	public File storeFile(MultipartFile file) {
		String fileName = file.getOriginalFilename();
		File dbFile = null;
		try {
			dbFile = new File(fileName, file.getContentType(), this.fileStorageLocation+"/" + fileName);
			Path targetLocation = this.fileStorageLocation.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return fileRepository.save(dbFile);

	}
}
