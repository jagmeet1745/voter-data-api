package com.kellton.voterreg;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import com.kellton.voterreg.property.FileStorageProperties;

import it.ozimov.springboot.mail.configuration.EnableEmailTools;

@EnableEmailTools
@SpringBootApplication
@EnableConfigurationProperties({ FileStorageProperties.class })
public class VoterEnrollmentDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(VoterEnrollmentDataApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
